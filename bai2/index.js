function loiChao() {
  var selectValue = document.getElementById("select").value;
  var result = document.getElementById("result");

  switch (selectValue) {
    case "B":
      result.innerHTML = `Xin chào bố !`;
      break;
    case "M":
      result.innerHTML = `Xin chào mẹ !`;
      break;
    case "ET":
      result.innerHTML = `Xin chào em trai !`;
      break;
    case "EG":
      result.innerHTML = `Xin chào em gái !`;
      break;

    default:
      result.innerHTML = `Xin chào người lạ !`;
      break;
  }
}
