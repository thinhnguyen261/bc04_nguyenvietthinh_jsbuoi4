function sapXep() {
  var soThuNhat = document.getElementById("txt-so-thu-nhat").value * 1;
  var soThuHai = document.getElementById("txt-so-thu-hai").value * 1;
  var soThuBa = document.getElementById("txt-so-thu-ba").value * 1;
  var result = document.getElementById("result");

  if (soThuNhat > soThuHai) {
    if (soThuHai > soThuBa) {
      result.innerHTML = `${soThuBa} < ${soThuHai} < ${soThuNhat}`;
    } else if (soThuHai < soThuBa) {
      if (soThuNhat > soThuBa) {
        result.innerHTML = `${soThuHai} < ${soThuBa} < ${soThuNhat}`;
      } else if (soThuNhat < soThuBa) {
        result.innerHTML = `${soThuHai} < ${soThuNhat} < ${soThuBa}`;
      }
    }
  } else if (soThuNhat < soThuHai) {
    if (soThuHai < soThuBa) {
      result.innerHTML = `${soThuNhat} < ${soThuHai} < ${soThuBa}`;
    } else if (soThuBa < soThuHai) {
      if (soThuNhat > soThuBa) {
        result.innerHTML = `${soThuBa} < ${soThuNhat} < ${soThuHai}`;
      } else if (soThuNhat < soThuBa) {
        result.innerHTML = `${soThuNhat} < ${soThuBa} < ${soThuHai}`;
      }
    }
  }
}
